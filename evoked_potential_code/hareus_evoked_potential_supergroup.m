function  hareus_evoked_potential_supergroup
date_time_string        = datestr(datetime,'YYYY_mm_dd-HH_MM_SS');
subject                 = 'EP020_KP';
stimulation_log_name    = [subject '_evoked_potential_test_' date_time_string]; 
fid                     = fopen(stimulation_log_name,'w');

duration            = 300;
amplitude           = 1000;
n_trials            = 1;
%%%%%%%%%%
% Set pulse parameters for bipolar  pulses
%%%%%%%%%%
width_a             = 100;
width_b             = width_a;
interphase          = 53;
system_frequency  	= 100; % Keep this fixed for asynchronous
ep_frequency        = 1;
n_pulses            = duration * ep_frequency; 

%%%%%%%%%%
% Configure experiment
%%%%%%%%%%
stim_contact    = [2 8 14 20];
% stim_contact    = [1 7 13 19];
amplitudes      = [1000];


% Configure the stimulator
stimulator = configure_stimulator(1);

% Setup the cleanUp function
finishup    = onCleanup(@() clean_up(stimulator));

% Program our stimulation waveforms 
CF = 0;
AF = 1;
stimulator.setStimPattern('waveform',1,...  % We can define multiple waveforms and distinguish them by ID
    'polarity',AF,...                        % 0=CF, 1=AF
    'pulses',1,...                          % Number of pulses in stim pattern
    'amp1',amplitude,...                    % Amplitude in uA
    'amp2',amplitude,...                    % Amplitude in uA
    'width1',width_a,...                    % Width for first phase in us
    'width2',width_b,...                    % Width for second phase in us
    'interphase',interphase,...             % Time between phases in us
    'frequency',system_frequency);                 % Frequency determines time between biphasic pulses

% Program our stimulation waveforms 
stimulator.setStimPattern('waveform',2,...  % We can define multiple waveforms and distinguish them by ID
    'polarity',CF,...                      	% 0=CF, 1=AF
    'pulses',1,...                          % Number of pulses in stim pattern
    'amp1',amplitude,...                    % Amplitude in uA
    'amp2',amplitude,...                    % Amplitude in uA
    'width1',width_a,...                    % Width for first phase in us
    'width2',width_b,...                    % Width for second phase in us
    'interphase',interphase,...             % Time between phases in us
    'frequency',system_frequency);                 % Frequency determines time between biphasic pulses

for c1 = 1:n_trials
    
    % Create a program sequence using any previously defined waveforms
    stimulator.beginSequence;       % Begin program definition
    stimulator.autoStim(stim_contact(1),1);      
    stimulator.autoStim(stim_contact(2),1);      
    stimulator.autoStim(stim_contact(3),2);      
    stimulator.autoStim(stim_contact(4),2);      
    stimulator.endSequence;         % End program definition

    cbmex('open')
    t_start = cbmex('time');

    % Play our program; number of repeats
    for c2 = 1:n_pulses
        stimulator.play(1);
      	t_start = cbmex('time');
        pause(1/ep_frequency);
        stimulator.stop();

        fprintf(fid, '%d, %f, %.4f, %.4f, %d, %.4f, %.4f\n', ...
             c2, t_start, duration, amplitude, 252, system_frequency, ep_frequency);

        fprintf('pulse=%d/%d, time=%.2f\n',c2, n_pulses, t_start)
    end
    cbmex('close')

end

fclose(fid);
end

function stimulator = configure_stimulator(n_modules)

% Create stimulator object
stimulator = cerestim96();

% Check for stimulation
DeviceList = stimulator.scanForDevices();

% Select a stimulator
stimulator.selectDevice(DeviceList(1));

% Connect to the stimulator
stimulator.connect; 

% Activate modules
d_info = stimulator.deviceInfo();
for c1 = 1:n_modules
    if d_info.moduleStatus(c1) ~= 1
        stimulator.enableModule(c1)
    end
end

end

function clean_up(stimulator)
stimulator.stop()
stimulator.disconnect
cbmex('close')
end


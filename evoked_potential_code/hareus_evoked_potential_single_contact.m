function  hareus_evoked_potential
date_time_string        = datestr(datetime,'YYYY_mm_dd-HH_MM_SS');
subject                 = 'KP';
stimulation_log_name    = [subject '_evoked_potential_test_' date_time_string]; 
fid                     = fopen(stimulation_log_name,'w');

train_interval_on   = 10;
train_interval_off  = 5;
duration            = 12;
amplitude           = 1000;

%%%%%%%%%%
% Set pulse parameters for bipolar lilly pulses
%%%%%%%%%%
width_a         = 100;
width_b         = width_a;
interphase      = 53;
frequency       = 16; % Keep this fixed for asynchronous
n_pulses        = train_interval_on * frequency;
n_repetitions   = 1;
%%%%%%%%%%
% Configure experiment
%%%%%%%%%%
contacts    = 2:2:24;
amplitudes  = [1000];

input_space     = combvec(contacts, amplitudes)';
parameter_list  = []; 
for c1 = 1:n_repetitions
    perm_idx        = randperm(size(input_space,1));
    parameter_list  = [parameter_list; input_space(perm_idx,:)];
end


% Configure the stimulator
stimulator = configure_stimulator(1);

% Setup the cleanUp function
finishup    = onCleanup(@() clean_up(stimulator));

for c1 = 1:size(parameter_list,1)
    stim_contact    = parameter_list(c1,1);
    stim_amplitude  = parameter_list(c1,2);
    
    % Program our stimulation waveforms 
    stimulator.setStimPattern('waveform',1,...  % We can define multiple waveforms and distinguish them by ID
        'polarity',0,...                        % 0=CF, 1=AF
        'pulses',n_pulses,...                   % Number of pulses in stim pattern
        'amp1',stim_amplitude,...               % Amplitude in uA
        'amp2',stim_amplitude,...               % Amplitude in uA
        'width1',width_a,...                    % Width for first phase in us
        'width2',width_b,...                    % Width for second phase in us
        'interphase',interphase,...             % Time between phases in us
        'frequency',frequency);                 % Frequency determines time between biphasic pulses

    
        % Create a program sequence using any previously defined waveforms
        stimulator.beginSequence;       % Begin program definition
        stimulator.autoStim(stim_contact,1);      
        stimulator.endSequence;         % End program definition
        
        cbmex('open')
        t_start = cbmex('time');

        % Play our program; number of repeats
        stimulator.play(1);
        pause(train_interval_on);
        stimulator.stop();

        fprintf(fid, '%f, %.2f, %.2f, %d, %.2f, %.2f, %.2f\n', ...
             t_start, duration, amplitude, 252, frequency, train_interval_on, train_interval_off);

        fprintf('time=%f, frequency=%d, train_interval_on=%d, train_interval_off=%d, duration=%d, pattern=CLINICAL\n', ...
            t_start, frequency, train_interval_on, train_interval_off, duration)
        cbmex('close')

    pause(train_interval_off);
end


fclose(fid)
end

function stimulator = configure_stimulator(n_modules)

% Create stimulator object
stimulator = cerestim96();

% Check for stimulation
DeviceList = stimulator.scanForDevices();

% Select a stimulator
stimulator.selectDevice(DeviceList(1));

% Connect to the stimulator
stimulator.connect; 

% Activate modules
d_info = stimulator.deviceInfo();
for c1 = 1:n_modules
    if d_info.moduleStatus(c1) ~= 1
        stimulator.enableModule(c1)
    end
end

end

function clean_up(stimulator)
stimulator.stop()
stimulator.disconnect
cbmex('close')
end


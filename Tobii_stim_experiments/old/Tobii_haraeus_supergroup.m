function  haraeus_synchronized(train_interval_on, train_interval_off, amplitude, duration, synch_frequency, fid)

subject_id          = 'KP';
experiment_date     = datestr(datetime, 'yyyy-mm-dd_HH_MM_SS');
log_path            = ['stim_logs/' subject_id '/' subject_id '_' experiment_date '.csv'];
fid                 = fopen(log_path, 'a');


train_interval_on   = 5;
train_interval_off  = 5;
duration            = 1200;
amplitude           = 1000;
synch_frequency     = 130; % this one does work

%%%%%%%%%%
% Set pulse parameters for bipolar symmetric pulses
%%%%%%%%%%
width_a     = 200;
width_b     = width_a;
amp_a       = amplitude;
amp_b       = amp_a;
interphase  = 53;
frequency   = 130; 
n_pulses    = 1;

%%%%%%%%%%
% Configure asynchronous pulse train
%%%%%%%%%%

% Default order of implanted electrode
anterior_ventral        = 21;
anterior_dorsal         = 13;
posterior_ventral       = 2;
posterior_dorsal        = 22;

n_modules               = 4; % This should be derived from the number of banks

%%%%%%%%%%
% Configure experiment
%%%%%%%%%%
n_repetitions           = floor(duration / (train_interval_on + train_interval_off)); % Times pulse train + interval are repeated

% Configure the stimulator
stimulator = configure_stimulator(n_modules);

% Setup the cleanUp function
finishup    = onCleanup(@() clean_up(stimulator));

% Program our stimulation waveforms 
stimulator.setStimPattern('waveform',1,...  % We can define multiple waveforms and distinguish them by ID
    'polarity',0,...                        % 0=CF, 1=AF
    'pulses',n_pulses,...                   % Number of pulses in stim pattern
    'amp1',amp_a,...                        % Amplitude in uA
    'amp2',amp_b,...                        % Amplitude in uA
    'width1',width_a,...                    % Width for first phase in us
    'width2',width_b,...                    % Width for second phase in us
    'interphase',interphase,...             % Time between phases in us
    'frequency',frequency);                 % Frequency determines time between biphasic pulses

stimulator.setStimPattern('waveform',2,...  % We can define multiple waveforms and distinguish them by ID
    'polarity',1,...                        % 0=CF, 1=AF
    'pulses',n_pulses,...                   % Number of pulses in stim pattern
    'amp1',amp_a,...                        % Amplitude in uA
    'amp2',amp_b,...                        % Amplitude in uA
    'width1',width_a,...                    % Width for first phase in us
    'width2',width_b,...                    % Width for second phase in us
    'interphase',interphase,...             % Time between phases in us
    'frequency',frequency);                 % Frequency determines time between biphasic pulses


% Create a program sequence using any previously defined waveforms
stimulator.beginSequence;       % Begin program definition
stimulator.autoStim(anterior_dorsal,1);      
stimulator.autoStim(anterior_ventral,2);
stimulator.autoStim(posterior_dorsal,1);      
stimulator.autoStim(posterior_ventral,2);
stimulator.endSequence;         % End program definition

%%%%%%%
% 
%%%%%%%
for c1 = 1:n_repetitions
        
    cbmex('open')
    t_start = cbmex('time');
    
    % Play our program; number of repeats
    stimulator.play(0);
    pause(train_interval_on);
    stimulator.stop();

%     fprintf(fid, '%f, %.2f, %.2f, %d, %.2f, %.2f, %.2f, synchronous, anterior_dorsal=%d, anterior_ventral=%d, posterior_dorsal=%d, posterior_ventral=%d\n', ...
%          t_start, duration, amplitude, 452, synch_frequency, train_interval_on, train_interval_off, anterior_dorsal, anterior_ventral, posterior_dorsal, posterior_ventral );

    fprintf('time=%f, frequency=%d, train_interval_on=%d, train_interval_off=%d, duration=%d, pattern=synchronous\n', ...
        t_start, synch_frequency, train_interval_on, train_interval_off, duration)
    cbmex('close')

    pause(train_interval_off);

end

pause(duration)
end

function stimulator = configure_stimulator(n_modules)

% Create stimulator object
stimulator = cerestim96();

% Check for stimulation
DeviceList = stimulator.scanForDevices();

% Select a stimulator
stimulator.selectDevice(DeviceList(1));

% Connect to the stimulator
stimulator.connect; 

% Activate modules
d_info = stimulator.deviceInfo();
for c1 = 1:n_modules
    if d_info.moduleStatus(c1) ~= 1
        stimulator.enableModule(c1)
    end
end

end

function clean_up(stimulator)
stimulator.stop()
stimulator.disconnect
cbmex('close')
end



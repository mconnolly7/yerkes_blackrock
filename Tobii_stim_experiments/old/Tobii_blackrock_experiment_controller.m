function Tobii_blackrock_experiment_controller

EXPERIMENT          =1;
train_interval_on   = 60*100;
train_interval_off  = 0;

switch EXPERIMENT
    
    case 1 % Block-8 continuous
        bank_size = 8;
        Tobii_blackrock_macrostim_test(bank_size, train_interval_on, train_interval_off)
        
        
    case 2 % Dist_4 continuous
        Tobii_blackrock_micro_stim_test(train_interval_on, train_interval_off)
        
                                                                                                                                                        
    case 3 % Block-4 continuous
        bank_size = 4;       
        Tobii_blackrock_macrostim_test(bank_size, train_interval_on, train_interval_off)
        
end


function  Tobii_blackrock_micro_stim_test(train_interval_on, train_interval_off)
close all;
clc;

%%%%%%%%%%
% Set pulse parameters
%%%%%%%%%%
width_a     = 400;
width_b     = width_a;
amp_a       = 150;
amp_b       = amp_a;
interphase  = 53;
frequency   = 100; % Keep this fixed for asynchronous
n_pulses    = 1;
polarity    = 0;

%%%%%%%%%%
% Configure asynchronous pulse train
%%%%%%%%%%

% Default order of implanted electrode
bank_1                  = [23, 7, 10, 26, 25, 9, 8, 24];
bank_2                  = [21, 5, 12, 28, 27, 11, 6, 22];
bank_3                  = [19, 3, 14, 30, 29, 13, 4, 20];
bank_4                  = [17, 1, 16, 32, 31, 15, 2, 18];

n_modules               = 8; % This should be derived from the number of banks
n_channels              = size(bank_1,2);
asynch_train_freq       = 7;

% Derived values
pulse_duration          = width_a + width_b + interphase;
asynch_train_duration   = 1e6/asynch_train_freq;
inter_pulse             = asynch_train_duration/n_channels;
wait_duration           = 1000/(n_channels * asynch_train_freq) - 11; % Because it's in milliseconds...who knows why

% Seed the random number generator
rng(0)
stim_order              = randperm(n_channels);

%%%%%%%%%%
% Configure experiment
%%%%%%%%%%
train_duration          = train_interval_on; % Duration of asynchronous pulse train (seconds)
inter_train_duration    = train_interval_off; % Duration of inter-pulse-train-interval (seconds)
n_repetitions           = floor(train_duration / (train_interval_on + train_interval_off)); % Times pulse train + interval are repeated
n_experiments           = 1; % Number of intermittent ADMETS + SHAM

%%% Example Experiment Sequence %%%
% train_duration          = 3; % Duration of asynchronous pulse train (seconds)
% inter_train_duration    = 3; % Duration of inter-pulse-train-interval (seconds)
% n_repetitions           = 3; % Times pulse train + interval are repeated
% n_experiments           = 2; % Number of intermittent ADMETS + SHAM
%
%             EXPERIMENT 1                           EXPERIMENT 2
% ***---***---***--- ------------------  ***---***---***--- ------------------
%

% Configure the stimulator
stimulator = configure_stimulator(n_modules);

% Setup the cleanUp function
finishup    = onCleanup(@() clean_up(stimulator));

% Program our stimulation waveform 
stimulator.setStimPattern('waveform',1,...  % We can define multiple waveforms and distinguish them by ID
    'polarity',polarity,...                 % 0=CF, 1=AF
    'pulses',n_pulses,...                   % Number of pulses in stim pattern
    'amp1',amp_a,...                        % Amplitude in uA
    'amp2',amp_b,...                        % Amplitude in uA
    'width1',width_a,...                    % Width for first phase in us
    'width2',width_b,...                    % Width for second phase in us
    'interphase',interphase,...             % Time between phases in us
    'frequency',frequency);                 % Frequency determines time between biphasic pulses

% Create a program sequence using any previously defined waveforms (we only have one)

stimulator.beginSequence;                   % Begin program definition

for c1 = 1:n_channels
    stimulator.beginGroup
    
    channel_1 = bank_1(stim_order(c1));
    channel_2 = bank_2(stim_order(c1));
    channel_3 = bank_3(stim_order(c1));
    channel_4 = bank_4(stim_order(c1));
    
    % Headstage  1
    stimulator.autoStim(channel_1, 1);      
    stimulator.autoStim(channel_2, 1);
    stimulator.autoStim(channel_3, 1);      
    stimulator.autoStim(channel_4, 1);
    
    % Headstage  2
%     stimulator.autoStim(channel_1 + 32, 1);      
%     stimulator.autoStim(channel_2 + 32, 1);
%     stimulator.autoStim(channel_3 + 32, 1);      
%     stimulator.autoStim(channel_4 + 32, 1);
    
    stimulator.endGroup
    
    stimulator.wait(wait_duration);
end

stimulator.endSequence;         % End program definition
for c2 = 1:n_experiments
    for c1 = 1:n_repetitions
        
        % Play our program; number of repeats
        tic
        stimulator.play(0);
        while toc < train_duration
        
        end
        stimulator.stop()
        toc
        pause(inter_train_duration);

    end
    pause((train_duration + inter_train_duration)*n_repetitions)

end

% Close it all
cbmex('close')
stimulator.disconnect;
clear stimulator

end

function stimulator = configure_stimulator(n_modules)

% Create stimulator object
stimulator = cerestim96();

% Check for stimulation
DeviceList = stimulator.scanForDevices();

% Select a stimulator
stimulator.selectDevice(DeviceList(1));

% Connect to the stimulator
stimulator.connect; 

% Activate modules
d_info = stimulator.deviceInfo();
for c1 = 1:n_modules
    if d_info.moduleStatus(c1) ~= 1
        stimulator.enableModule(c1)
    end
end

end

function clean_up(stimulator)
stimulator.stop()
stimulator.disconnect
cbmex('close')
end
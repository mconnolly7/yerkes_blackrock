clc; close all; clear
% data_path = 'yerkes_blackrock/tobii_code/data_temp/processed_data/KP_AOI_SL/';
data_path = 'yerkes_blackrock/tobii_code/data_temp/processed_data/KP_BL_VD_AOI/';

d           = dir([data_path '*.mat']);
VPC_all     = [];

for c1 = 1:size(d,1)
    file_name = d(c1).name;
    
    load([data_path file_name]);
    
    if contains(file_name, 'Sham')
        stim_idx = 1*ones(size(VPC_mat,1),1);
    elseif contains(file_name, 'Standard')
        stim_idx = 2*ones(size(VPC_mat,1),1);     
    elseif contains(file_name, 'Ring')
        stim_idx = 3*ones(size(VPC_mat,1),1);                 
    elseif contains(file_name, 'Full')
        stim_idx = 4*ones(size(VPC_mat,1),1);
    else
        clear stim_idx
    end
    
    VPC_all = [VPC_all; stim_idx, VPC_mat];
end

valid_idx   = VPC_all(:,2) > 5 & VPC_all(:,2) < 35 ;

%% Compare DS
close all
DS          = (VPC_all(:,4) - VPC_all(:,3))./(VPC_all(:,4) + VPC_all(:,3));
DS_valid    = DS(valid_idx);

G           = VPC_all(:,1);
G_valid     = G(valid_idx==1,1);

p           = anova1(DS_valid, G_valid, 'off');
hold on

boxplot(DS_valid, G_valid, 'notch', 'on');
xticklabels({'Sham', 'Wide Bipolar', 'Ring Bipolar',  'Full ADMES'})
ylabel('DS')
set(gca,'FontSize', 16)

a       = get(get(gca,'children'),'children');   % Get the handles of all the objects
t       = get(a,'tag');   % List the names of all the objects 
med_ax  = strcmp(t, 'Median');

set(a, 'LineWidth', 2);   % Set the color of the first box to green
set(a(~med_ax), 'Color', 'k');   % Set the color of the first box to green

title(sprintf('P-value: %.4f', p))

%% Compare Raw A times
% G_valid         = VPC_all(:,1);
% p               = anova1(VPC_all(:,2), G_valid);
% xticklabels({'Sham', 'Clinical', 'Ring',  'Full'})
% ylabel('Raw A')
% set(gca,'FontSize', 16)
% title(sprintf('P-value: %.4f', p))

%% Compare valid trials
G_dummy = full(ind2vec(G'))';
mdl     = fitglm(G_dummy, valid_idx, 'Distribution','binomial')

for c1 = 1:4
    p(c1) = mean(valid_idx(G_valid == c1));
end

ax = figure
p_std   = p .* (1-p);
p_se    = sqrt(p_std ./ sum(G_dummy));

hold on
ax = bar(1:4,p,'FaceColor', .5*ones(1,3), 'LineWidth', 4);
errorbar(1:4,p,p_se, 'k.', 'LineWidth', 4)

xticks(1:4)
xticklabels({'Sham', 'Wide Bipolar', 'Ring Bipolar',  'Full ADMES'})
ylabel('Valid Trial Probability')
set(gca,'FontSize', 16)
% title(sprintf('P-value: %.4f', p))




% experiment_dir  = 'yerkes_blackrock/tobii_code/data_temp/processed_data/KP_BL_VD_AOI/';
experiment_dir  = 'yerkes_blackrock/tobii_code/data_temp/processed_data/KP_PCN_VD_AOI/';
d               = dir([experiment_dir 'KP*']);

DS_all          = [];
T_all           = [];
for c1 = 1:size(d,1)
    file_name   = d(c1).name;
    file_path   = [experiment_dir file_name];
    
    load(file_path)
    DS              = (VPC_mat(:,5) - VPC_mat(:,4))./(VPC_mat(:,5) + VPC_mat(:,4));
    T               = c1*ones(size(DS));
    DS_all          = [DS_all; DS];
    T_all           = [T_all; T];
    DS_mean(c1)     = nanmean(DS);
end


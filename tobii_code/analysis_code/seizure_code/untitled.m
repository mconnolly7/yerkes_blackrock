function DS_table = untitled

DS_table    = get_subject_data('Waldo');
DS_table    = [DS_table; get_subject_data('Casper')];

end

function DS_table = get_subject_data(subject)
%
%
%
switch subject
    case 'Casper'        
        data_dir        = '/Volumes/ADLab/share/Alejandra/Casper/D1/';
        data_table_path = [data_dir 'CasperDB.xlsx'];
    case 'Waldo'
        data_dir        = '/Volumes/ADLab/share/Alejandra/Waldo/';
        data_table_path = [data_dir 'Waldo Tobii DB.xlsx'];
end
pcn_table = readtable(data_table_path);

%%
d1      = dir(data_dir);
index   = 1;

for c1 = 1:size(d1,1)
    dir_name    = d1(c1).name;

    % Check for session ID
        
    trial_row           = strcmpi(pcn_table.BRRecordingNameForSound, dir_name);

    if all(trial_row==0)
        continue;
    end
    
    session_id(index)   = pcn_table.Session(trial_row);
    cum_pcn(index)      = pcn_table.CumulitivePCNU(trial_row);
    
    if d1(c1).isdir && ~strcmp(dir_name(1), '.')
        
        d2                      = dir([data_dir dir_name]);
        
        has_siezures(index)     = 0;
        has_interictal(index)   = 0;
        for c2 = 1:size(d2,1)
            file_name = d2(c2).name;
            
            if contains(file_name, 'seizure', 'IgnoreCase', true)
                has_siezures(index) = 1;
            end 
            
            if contains(file_name, 'interictal', 'IgnoreCase', true)
                has_interictal(index) = 1;
            end 
        end
        
        % Look up the session       
        index = index + 1;

    end
end


%%
close all
DS_table = [];
variable_names = {'subject', 'session_id', 'cum_pcn', 'has_seizures', 'has_interictal', 'visit_A', 'DS', 'A1', 'B1'};

for c1 = 1:size(session_id,2)
    tobii_data = [subject '_D1_' num2str(session_id(c1)) '_'];
    
    for c2 = 1:size(d1)        
        file_name = d1(c2).name;
        
        if contains(file_name, tobii_data) && contains(file_name, '.mat')
            load([data_dir file_name]);
            
            DS              = (data(:,6) - data(:,5))./(data(:,6) + data(:,5));   
            
            sz_idx          = repmat(has_siezures(c1), size(DS));
            ii_idx          = repmat(has_interictal(c1),size(DS));
            sid             = repmat(session_id(c1), size(DS));
            subject_mat     = repmat({subject}, size(DS));
            cummulative_pcn = repmat(cum_pcn(c1), size(DS));
            
            visit_time  = data(:,4);
            
            d_table     = table(subject_mat, sid, cummulative_pcn, sz_idx, ii_idx, data(:,4), DS, data(:,5), data(:,6),'VariableNames', variable_names);
            DS_table    = [DS_table; d_table];
        end        
    end   
end

nan_idx         = ~isnan(DS_table.DS);
visit_idx       = DS_table.visit_A > 5 & DS_table.visit_A < 35;

valid_data      = DS_table(nan_idx & visit_idx,:);

baseline_idx    = valid_data.session_id <= 10;

baseline_ds     = valid_data.DS(baseline_idx);
seizure_ds      = valid_data.DS(valid_data.has_seizures == 1);
interictal_ds   = valid_data.DS(valid_data.has_interictal == 1);

Y               = [baseline_ds; interictal_ds; seizure_ds];
G               = [ones(size(baseline_ds)); 2*ones(size(interictal_ds)); 3*ones(size(seizure_ds))];
% [h, p]          = ano(baseline_ds, seizure_ds);

m               = [mean(baseline_ds) mean(interictal_ds) mean(seizure_ds)];

hold on
h               = boxplot(Y,G, 'notch', 'on');
plot([1 2 3], m, 'x', 'MarkerSize', 10, 'Color', 'k', 'LineWidth', 2)
plot([-1 4], [0 0], 'LineWidth', 2, 'LineStyle', '--', 'color', [1 0 0]);

xticklabels({'Baseline', 'Interictal', 'Seizure'});
set(h, 'color', 'k')
set(h, 'LineWidth', 2)
ylabel('Discriminant Score')
set(gca,'FontSize', 16)
box off
end



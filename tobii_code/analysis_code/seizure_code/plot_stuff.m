close all
dark_2      = [27,158,119;
            217,95,2]/255;

nan_idx     = ~isnan(DS_table.DS);
visit_idx   = DS_table.visit_A >= 5 & DS_table.visit_A <= 35; 

valid_idx   = nan_idx;%& visit_idx;

ds_valid    = DS_table(valid_idx,:);

ds_waldo    = ds_valid(strcmp(ds_valid.subject, 'Waldo'),:);
ds_casper   = ds_valid(strcmp(ds_valid.subject, 'Casper'), :);

p_waldo     = polyfit(ds_waldo.cum_pcn, (ds_waldo.A1 + ds_waldo.B1)/.05,1);
yw          = polyval(p_waldo, ds_waldo.cum_pcn);

p_casper    = polyfit(ds_casper.cum_pcn, (ds_casper.A1 + ds_casper.B1)/.05,1);
yc          = polyval(p_casper, ds_casper.cum_pcn);

[rho(1), p(1)] = corr(ds_waldo.cum_pcn, (ds_waldo.A1 + ds_waldo.B1)/.05);
[rho(2), p(2)] = corr(ds_casper.cum_pcn, (ds_casper.A1 + ds_casper.B1)/.05);
hold on
scatter(ds_waldo.cum_pcn, (ds_waldo.A1 + ds_waldo.B1)/.05, 100, 'o', ...
    'MarkerFaceColor', dark_2(1,:), 'MarkerEdgeColor', 'k', 'MarkerFaceAlpha', .5, 'LineWidth', 2);
scatter(ds_casper.cum_pcn, (ds_casper.A1 + ds_casper.B1)/.05, 100, 'o', ...
    'MarkerFaceColor', dark_2(2,:), 'MarkerEdgeColor', 'k', 'MarkerFaceAlpha', .5, 'LineWidth', 2);

plot(ds_casper.cum_pcn, yc, 'LineWidth', 2, 'color', dark_2(2,:));
plot(ds_waldo.cum_pcn, yw, 'LineWidth', 2, 'color', dark_2(1,:));
xlim([-10000 max(ds_valid.cum_pcn)*1.1])

xlabel('Cumulative PCN')
ylabel('Percent time looking at screen')
ylim([0 110])
set(gca,'FontSize', 16);
legend({sprintf('NHP 1, rho =%.2f, p=%.2e', rho(1), p(1)), sprintf('NHP 2, rho =%.2f, p=%.2e', rho(2), p(2))})

%%

waldo_bl_a     =  ds_valid.A1(ds_valid.cum_pcn == 0 & strcmpi(ds_valid.subject,'Waldo'));
waldo_pc_a     =  ds_valid.A1(ds_valid.cum_pcn ~= 0 & strcmpi(ds_valid.subject,'Waldo'));
casper_bl_a    =  ds_valid.A1(ds_valid.cum_pcn == 0 & strcmpi(ds_valid.subject,'Casper'));
casper_pc_a    =  ds_valid.A1(ds_valid.cum_pcn ~= 0 & strcmpi(ds_valid.subject,'Casper'));

waldo_bl_b     =  ds_valid.B1(ds_valid.cum_pcn == 0 & strcmpi(ds_valid.subject,'Waldo'));
waldo_pc_b     =  ds_valid.B1(ds_valid.cum_pcn ~= 0 & strcmpi(ds_valid.subject,'Waldo'));
casper_bl_b    =  ds_valid.B1(ds_valid.cum_pcn == 0 & strcmpi(ds_valid.subject,'Casper'));
casper_pc_b    =  ds_valid.B1(ds_valid.cum_pcn ~= 0 & strcmpi(ds_valid.subject,'Casper'));


ds_means        = [mean(waldo_bl_ds) mean(waldo_pc_ds); mean(casper_bl_ds) mean(casper_pc_ds)];
ds_stds         = [std(waldo_bl_ds)/sqrt(size(waldo_bl_ds,1))...
    std(waldo_pc_ds)/sqrt(size(waldo_pc_ds,1)); ...
    std(casper_bl_ds)/sqrt(size(casper_bl_ds,1))...
    std(casper_pc_ds)/sqrt(size(casper_pc_ds,1))];


barwitherr(ds_stds,ds_means)
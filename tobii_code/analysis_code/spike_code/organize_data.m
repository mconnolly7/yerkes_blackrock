function organize_data
subject     = 'Casper';

data_dir    = ['data/data_organization/' subject '/'];
d           = dir(data_dir);
save_dir    = ['data/processed_data/' subject '/'];

for c1 = 1:size(d,1)
    d_name = d(c1).name;
    if ~d(c1).isdir || strcmp(d_name(1), '.')
        continue;
    end
    
    data_path                               = [data_dir d_name];
    [spike_data, sound_data, seizure_data, tobii_data]  = get_spike_data(data_path);
    
    if ~isempty(spike_data) ...
            && ~isempty(sound_data) ...
            && isempty(seizure_data)
        
        save_path = [save_dir d_name '.mat'];
        save(save_path,'spike_data', 'sound_data', 'tobii_data');
       
    end
       
end
end

function [spike_data, sound_data, seizure_data, tobii_table] = get_spike_data(data_path)
d = dir(data_path);

sound_data      = [];
spike_data      = [];
seizure_data    = [];

for c1 = 1:size(d,1)
    experiment_name = d(c1).name;
    
    try 
        if contains(experiment_name, 'interictal','IgnoreCase',true)  
            a               = load([data_path filesep experiment_name]);   
            fields          = fieldnames(a);
            
            for c2 = 1:size(fields,1)
                if contains(fields{c2}, '33') || contains(fields{c2}, '65')
                    spike_data      = a.(fields{c2});
                end
            end
       
        elseif contains(experiment_name, 'sound','IgnoreCase',true)
            a               = load([data_path filesep experiment_name]);   
            fields          = fieldnames(a);
            sound_data      = a.(fields{1});
            
        elseif contains(experiment_name, 'seizure','IgnoreCase',true)
            a               = load([data_path filesep experiment_name]);
            fields          = fieldnames(a);
            seizure_data    = a.(fields{1});
            
        elseif contains(experiment_name, 'D1','IgnoreCase',false)
            a               = load([data_path filesep experiment_name]);
            tobii_data      = a.data;              
            table_header    = a.Table;
            
            for c2 = 1:size(table_header,2)
                table_header{c2} = strrep(table_header{c2},' ','_');
            end
            
            tobii_table     = array2table(tobii_data, 'VariableNames', table_header);
        end
    catch
        % Nothing to do
    end
end

end





function data_table = align_features(subject)

data_dir        = ['data/processed_data/' subject filesep];
d               = dir([data_dir '*.mat']);

% table_dir       = 'data/data_tables/';
% pcn_table       = readtable([table_dir subject '_data.xlsx']);

ephys_names     = {'experiment', 'trial', 'training_time_s', 'training_spikes', 'blank_spikes', 'testing_spikes'};
ephys_table     = table([],[],[],[],[],[], 'VariableNames', ephys_names);
tobii_table     = [];

for c1 = 1:size(d,1)
    f_name          = d(c1).name;   
    load([data_dir f_name]);
    
%     days_since_pcn  = pcn_table.DaysSincePCN(strcmp(pcn_table.BRRecordingNameForSound, f_name(1:end-4)));
%     cummulative_pcn = pcn_table.CumulitivePCNU(strcmp(pcn_table.BRRecordingNameForSound, f_name(1:end-4)));
    spike_times     = spike_data.times;

    trial = 1;
    for c2 = 1:2:size(sound_data.times,1)
        training_start  = sound_data.times(c2);
        training_end    = sound_data.times(c2+1);
        training_time   = training_end - training_start;
        
        n_training      = get_n_spikes(spike_times, training_start, training_end);
        
        blank_start     = training_end;
        blank_end       = blank_start + 10;       
        n_blank         = get_n_spikes(spike_times, blank_start, blank_end);
        
        test_start      = blank_end;
        test_end        = test_start + 5;       
        n_test          = get_n_spikes(spike_times, test_start, test_end);
        
        d_table         = table({f_name}, trial, training_time, n_training, ...
            n_blank, n_test, 'VariableNames', ephys_names);       
        
        ephys_table     = [ephys_table; d_table];
        
        trial           = trial+1;
    end
    
    tobii_table = [tobii_table; tobii_data];

end

switch subject
    case 'Waldo'
        data_table = [ephys_table tobii_table(1:end,:)];
    case 'Casper'
        data_table = [ephys_table tobii_table(2:end,:)];
end
end

function n_spikes = get_n_spikes(spike_times, window_start, window_end)
 
spike_idx_left  = find(spike_times > window_start);
spike_idx_right = find(spike_times < window_end);
spike_idx       = intersect(spike_idx_left, spike_idx_right);
        
n_spikes        = size(spike_idx,1);

end
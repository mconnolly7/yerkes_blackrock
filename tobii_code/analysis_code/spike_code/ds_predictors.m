

data_table_waldo    = align_features('Waldo');
data_table_casper   = align_features('Casper');

subject_table       = table([ones(size(data_table_waldo,1),1); 2*ones(size(data_table_casper,1),1)], 'VariableNames',{'subject_id'});
data_table          = [subject_table [data_table_waldo; data_table_casper]]; 

% Calculate DS
ds                  = (data_table.B1-data_table.A1)./(data_table.B1+data_table.A1);

training_spike_rate = data_table.training_spikes./data_table.training_time_s;

total_spike_rate    = (data_table.training_spikes + data_table.blank_spikes + data_table.testing_spikes) ...
                        ./(data_table.training_time_s + 15) ;

data_table          = [data_table table(ds, training_spike_rate, total_spike_rate, ...
                    'VariableNames', {'ds', 'training_spike_rate', 'total_spike_rate'})];

% Filter for visit time [5 35]
visit_valid_idx    = data_table.vist_rawA > 5 & data_table.vist_rawA < 35;

% Filter for training time
training_valid_idx  = data_table.training_time_s < 300;

% Remove nan
nan_valid_idx       = ~isnan(ds);

valid_idx           = visit_valid_idx & nan_valid_idx;
data_valid          = data_table(valid_idx,:);
data_invalid        = data_table(~valid_idx,:);

%%
clc
%%%%
% Training spike rate
%%%%

fprintf('Training spike rate\n')

% -> DS
[rho, p] = corr(data_valid.training_spike_rate, data_valid.ds);
fprintf('%20s p=%-20.4f rho=%-20.4f\n','DS', p, rho)

% -> training time
[rho, p] = corr(data_valid.training_spike_rate, data_valid.training_time_s);
fprintf('%20s p=%-20.4f rho=%-20.4f\n','training time', p, rho)

% -> valid trials
[h, p]  = ttest2(data_valid.training_spike_rate, data_invalid.training_spike_rate);
fprintf('%20s p=%-20.4f\n\n','Valid trials', p)

%%%%
% Blank spikes
%%%%

fprintf('Blank spike rate\n')

% -> DS
[rho, p] = corr(data_valid.blank_spikes, data_valid.ds);
fprintf('%20s p=%-20.4f rho=%-20.4f\n','DS', p, rho)

% -> valid trials
[~, p]  = ttest2(data_valid.blank_spikes, data_invalid.blank_spikes);
fprintf('%20s p=%-20.4f\n\n','Valid trials', p)


%%%%
% Test spikes
%%%%

fprintf('Test spike rate\n')

% -> DS
[rho, p] = corr(data_valid.testing_spikes, data_valid.ds);
fprintf('%20s p=%-20.4f rho=%-20.4f\n','DS', p, rho)

% -> valid trials
[~, p]  = ttest2(data_valid.testing_spikes, data_invalid.testing_spikes);
fprintf('%20s, p=%-20.4f\n\n','Valid trials', p)

%%%%
% Total spike rate
%%%%
fprintf('Total spike rate\n')

% -> DS
[~, p]    = corr(data_valid.total_spike_rate, data_valid.ds);
fprintf('%20s, p=%-20.4f, rho=%-10.4f\n','DS', p, rho)

% -> saccade index
[rho, p]    = corr(data_table.total_spike_rate, data_table.saccade_index);
fprintf('%20s, p=%-20.4f, rho=%-10.4f\n','Saccade index', p, rho)

% -> saccade duration
dur_idx     = isnan(data_table.saccade_duration);
[rho, p]    = corr(data_table.total_spike_rate(~dur_idx), data_table.saccade_duration(~dur_idx));
fprintf('%20s, p=%-20.4f, rho=%-10.4f\n','Saccade duration', p, rho)

% -> fixation index
[rho, p]    = corr(data_table.total_spike_rate, data_table.fixation_index);
fprintf('%20s, p=%-20.4f, rho=%-10.4f\n','Fixation index', p, rho)

% -> fixation duration
dur_idx     = isnan(data_table.fixation_duration);
[rho, p]    = corr(data_table.total_spike_rate(~dur_idx), data_table.fixation_duration(~dur_idx));
fprintf('%20s, p=%-20.4f, rho=%-10.4f\n','Fixation duration', p, rho)

% -> valid trials
[~, p]      = ttest2(data_valid.total_spike_rate, data_invalid.total_spike_rate);
fprintf('%20s, p=%-20.4f\n','Valid trials', p)

%%%%
% Days since PCN
%%%%

% -> DS

% -> saccade index

% -> saccade duration

% -> fixation index

% -> fixation duration


%%%%
% Cummulative PCN
%%%%

% -> DS

% -> saccade index

% -> saccade duration

% -> fixation index

% -> fixation duration

return
%%
close all
dark_2      = [27,158,119;
    217,95,2]/255;

dur_idx     = isnan(data_table.fixation_duration);
M1_idx      = data_table.subject_id == 1;
M2_idx      = data_table.subject_id == 2;

data_ts     = data_table.total_spike_rate;
data_fd     = data_table.fixation_duration;

[rho, p]    = corr(data_ts(~dur_idx), data_fd(~dur_idx));
theta       = polyfit(data_ts(~dur_idx), data_fd(~dur_idx), 1);
y           = polyval(theta, data_ts(~dur_idx));

figure;
hold on

scatter(data_ts(M2_idx & ~dur_idx), data_fd(M2_idx & ~dur_idx), 100, 'o', ...
    'MarkerFaceColor', dark_2(2,:), 'MarkerEdgeColor', 'k', 'MarkerFaceAlpha', .5, 'LineWidth', 2)

scatter(data_ts(M1_idx & ~dur_idx), data_fd(M1_idx & ~dur_idx), 100, 'o', ...
    'MarkerFaceColor', dark_2(1,:), 'MarkerEdgeColor', 'k', 'MarkerFaceAlpha', .5, 'LineWidth', 2)


plot(data_ts(~dur_idx), y, 'LineWidth', 2, 'color', 'k');

xlabel('Total Spike Rate')
ylabel('Fixation duration')
title(sprintf('rho = %.2f, p-value = %.2e', rho, p))
set(gca, 'FontSize', 16);


%%
close all

figure; hold on
h       = boxplot(data_table.total_spike_rate, valid_idx, 'notch', 'on');
[~, p]  = ttest2(data_table.total_spike_rate(~valid_idx), data_table.total_spike_rate(valid_idx));

m       = [mean(data_table.training_spike_rate(~valid_idx)) mean(data_table.training_spike_rate(valid_idx))];
plot([1 2], m, 'x', 'MarkerSize', 10, 'Color', 'k', 'LineWidth', 2)
xticklabels({'Valid Trials', 'Invalid Trials'});
set(h, 'color', 'k')
set(h, 'LineWidth', 2)
ylabel('Interictal Spike Frequency (spk/s)')

title(sprintf('p-value = %.2f', p))
set(gca, 'FontSize', 16);


% Question

% Independent: spike rate, cummulative PCN, days since PCN
% Dependent: 




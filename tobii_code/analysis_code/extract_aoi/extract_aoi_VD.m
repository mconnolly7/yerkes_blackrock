
filename        = 'KP_PCN_VD.txt';

file_id         = fopen(filename,'r');
save_path       = 'yerkes_blackrock/tobii_code/data_temp/processed_data/KP_PCN_VD_AOI/';
while ~feof(file_id)
    fname               = fgetl(file_id)
    
    % Select path based on operating system
    if ispc
        data_path               = '\\\\yrknas.yerkes.emory.edu\yerkes2\ADLab\ADMES\Tobii VPC\Tobii VPC\KP_Results\KP_Tobii Data\';       
    elseif ismac
        data_path               = '/Volumes/ADLab/ADMES/Tobii VPC/Tobii_VPC/Results/KP_Results/KP_Tobii_Data/';
    end
    
    % Load AOI data
    table_path                  = [data_path 'KP_AOI_data' filesep fname '.xlsx'];
%     table_path                  = [data_path 'KP_AOI_data' filesep fname '_AOI.xlsx'];
    VPC                         = readtable(table_path);
    
    % Reshape to create matrix
    % column is trial
    % rows are image/phase      
    VPC_mat_unordered           = reshape(VPC{1,2:end}, 5, [])';
    image_names                 = VPC.Properties.VariableNames(2:5:end);
    
    % Resort the matrix based on order of presentation
    clear image_idx
    for c1 = 1:size(image_names,2)
        [start_idx, end_idx]    = regexp(image_names{c1}, '\d+');
        image_idx(c1)           = str2double(image_names{c1}(start_idx:end_idx));     
    end
    if contains(fname, '34')
        fname;
    end
        
    [~, image_reorder]  = sort(image_idx);
    VPC_mat             = VPC_mat_unordered(image_reorder,:);
    
    save_file_name      = [fname '_processed'];
    save_file_path      = [save_path save_file_name];
    save(save_file_path, 'VPC_mat');
end
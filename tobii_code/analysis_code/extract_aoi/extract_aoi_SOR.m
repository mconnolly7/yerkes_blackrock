clc; clear; close all
subject         = 'Jaffar';
tobii_dir       = '/Volumes/ADLab/ADMES/Tobii VPC/';
SOR_dir         = [tobii_dir 'Tobii_SOR/'];
subject_dir     = [SOR_dir subject '_SOR_data/'];
aoi_dir         = [subject_dir subject '_SOR_AOI/'];

d               = dir([aoi_dir subject '*']);

data_all        = [];    

for c1 = 1:size(d,1)
    fname               = d(c1).name;
    
    % Load AOI data
    image_table                 = readtable([aoi_dir filesep fname]);  
    image_names                 = image_table.Properties.VariableNames(2:end);
    
    for c2 = 1:size(image_names,2)
        
        image_tokens        = strsplit(image_names{c2}, '_');
        data_table(c2,1)    = c1;
        
        if strcmp(image_tokens{4}(1), 'A')
            data_table(c2,2) = 0;
        else
            data_table(c2,2) = 1;
        end
        
        data_table(c2,3)    = str2num(image_tokens{4}(2:end));
        
        if contains(image_tokens{7}(2:end),'Y')
            data_table(c2,4) = 0;
        elseif contains(image_tokens{7}(2:end),'N')
            data_table(c2,4) = 1;
        else
            data_table(c2,4) = -1;
        end
        
        data_table(c2,5) = image_table{2,c2+1};
        
    end
    
data_all = [data_all; data_table];    
    

end    
    
test_idx    = data_all(:,2) == 1;
test_table  = data_all(test_idx,[1 3 4 5]);

trial       = unique(test_table(:,1:2), 'rows');

for c1 = 1:size(trial,1)
    trial_idx       = test_table(:,1) == trial(c1,1) & test_table(:,2) == trial(c1,2);
    
    trial_table     = test_table(trial_idx,:);
    moved_idx       = trial_table(:,3) == 0;
    stationary_idx  = trial_table(:,3) == 1;
    
    aoi_moved       = mean(trial_table(moved_idx,4));
    aoi_stationary  = mean(trial_table(stationary_idx,4));
    DS(c1)          = (aoi_moved - aoi_stationary)/(aoi_moved + aoi_stationary);
end

subplot(1,3,1)
boxplot(DS(1:10), 'Notch', 'on')
title('Session 1')
ylim([-1 1]);
ylabel('DS')
set(gca,'fontSize', 16);

subplot(1,3,2)
boxplot(DS(11:20), 'Notch', 'on')
title('Session 2')
ylim([-1 1]);
set(gca,'fontSize', 16);

subplot(1,3,3)
boxplot(DS, 'Notch', 'on')
title('All');
ylim([-1 1]);
set(gca,'fontSize', 16);


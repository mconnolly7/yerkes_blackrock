% function VPC_withVariableDelay_Sac(filename)
% This function will read the text file with all the name on it and do the
% data analysis of the data exported from the tobii
% Data containing the AOI in the (Data are on "Waldo_visit_raw" file) and the data with the total time
% spent on looking at A (data on the "Waldo_total_image_duration")
% A new file will be created with the combined values with the name on the
% txt file
% In Memory we only keep the trial were the monkey looked in between 5 and 35 in s),
% In attention we will compare all the trial for T and A.
%
% modified 07/30/2018 AD
% modified 09/05/2018 AD% fid2 = fopen(bfname,'r');
% i=1;
% while ~feof(fid2),
%     fname = fgetl(fid2)
%    load(fname);

% modified 10/29/2018 AD I added a colomn for the delay. It will be automatically saved in data
% example VPC_withVariableDelay('KP_Bl1_ShamStimD1VD.txt')

%careful for the VPC_VD the order of the AOI exported is
%:10,11,12,..19,1,20,2,...9

%Modified 01212019 by AD to accomodate the new seccade and fixation export
%   -calculate VPC
%   -extract the presentation time
%   -extract the delay in between A and B


clc
filename = 'KP_BL_VD.txt';
fid2 = fopen(filename,'r');
% while ~feof(fid2)
    fname               = fgetl(fid2);
    
    if ispc
        VPC                 = xlsread(['\\\\yrknas.yerkes.emory.edu\yerkes2\ADLab\ADMES\Tobii VPC\Tobii VPC\KP_Results\KP_Tobii Data\KP AOI data\' fname '_AOI']);
        [num1,txt1,raw1]    = xlsread(['\\yrknas.yerkes.emory.edu\yerkes2\ADLab\ADMES\Tobii VPC\Tobii VPC\KP_Results\KP_Tobii_Data\KP Sac Data\' fname '_Sac'],'E:J');
        [num2,txt2,raw2]    = xlsread(['\\yrknas.yerkes.emory.edu\yerkes2\ADLab\ADMES\Tobii VPC\Tobii VPC\KP_Results\KP_Tobii_Data\KP Sac Data\' fname '_Sac'],'DG:DK');
        raw                 = [raw1,raw2];
        
    elseif ismac
        data_path       = 'yerkes_blackrock/tobii_code/data_temp/';
%         data_path       = '/Volumes/ADLab/ADMES/Tobii VPC/Tobii_VPC/KP_Results/KP_Tobii_Data/';
        VPC             = xlsread([data_path 'KP_AOI_data' filesep fname '_AOI']);
        
        data_table      = readtable([data_path 'KP_sac_data' filesep fname '_Sac.xlsx']);
        raw             = [data_table(:,5:10) data_table(:,111:115)];
        
    end
    
    

    %% for the VPC task
    Min     = 5;
    Max     = 35;
    [a,b]   = size(VPC);
    i       = 1;
    j       = 1;
    n       = 1;
    image   = [];
    
    for i=1:5:(b-4)
        Fam20{j}    = VPC(1,i);
        i           = i+1;
        
        VPC_Fam1{j} = VPC(1,i);
        i           = i+1;
        
        VPC_New1{j} = VPC(1,i);
        i           = i+1;
        
        VPC_Fam2{j} = VPC(1,i);
        i           = i+1;
        
        VPC_New2{j} = VPC(1,i);
        
        i           = i+1;
        j           = j+1;
        image       = [image,n];
        n           = n+1;
    end
    
    VPC_Fam1                    = cat (1,VPC_Fam1{:});
    VPC_Fam2                    = cat (1,VPC_Fam2{:});
    VPC_New1                    = cat (1,VPC_New1{:});
    VPC_New2                    = cat (1,VPC_New2{:});
    Fam20                       = cat (1,Fam20{:});
    
    % Replace Nan values by zero
    VPC_Fam1(isnan(VPC_Fam1))   = 0;
    VPC_Fam2(isnan(VPC_Fam2))   = 0;
    VPC_New1(isnan(VPC_New1))   = 0;
    VPC_New2(isnan(VPC_New2))   = 0;
    Fam20(isnan (Fam20))        = 0;
    
    dat                         = [Fam20,VPC_Fam1,VPC_New1,VPC_Fam2,VPC_New2];
    
    %% Total duration of the task
    TTDuration                  = raw(2,1);
    
    %% Attention Delay Fixation and saccade
    % extract time looked at and delay and divided data by images but first
    % divide data for each images
    image_list          = data_table.MediaName;
    for i=1:(n-1)
        image_name      = ['VPC' num2str(i) 'a.png'];
        index_ImageA{i} = find(strcmp(image_list,image_name));
    end
    
    
    % for each image the all data set is stock in the variable Image
    for i=1:(n-2)
        X               = (index_ImageA{i});
        Y               = (index_ImageA{i+1});
        Image_data{i}   = raw(X(1):(Y(1)-1),:);
    end
    
    X                   = index_ImageA{i};
    Image_data{i}       = raw(X(1):size(raw,1),:);
    
    for i=1:size(Image_data,2)
        image_name      = ['VPC' num2str(i) 'b.png'];
        X               = Image_data{i};
        
        presentation{i} = ((X{length(index_ImageA{i}),3})-(X{1,3}))/1000; % data are in millisecond but we convert in s
        index_ImageB    = find(strcmp(X.MediaName,image_name));
        delay{i}        = (X{(index_ImageB(1)-1),3}-X{length(index_ImageA{i}),3})/1000; % delay include the bip sound %data are in millisecond but we convert in s that's for the delay

    end
    
    
    % Adjust the data from VPC because 10 is first and order changes if more
    % than 10 pictures
    Presentation    = cat(1,presentation{:});
    Delay           = cat(1,delay{:});
    
    if length (Presentation)<11
        dat2    = [dat((2:end),:);dat(1,:)]; % The first column is in fact image 10
    else
        dat2    = [dat(11,:);dat((13:20),:);dat((1:10),:);dat(12,:)]; % See the weird order in comment
    end
    
    %% extract the fixation index per image and the average duration
    
    clear Average_fixation_duration Average_saccade_duration Fixation_index saccade_dura
    i=1;
    im=1;
    for im=1:size(Image_data,2)
        X               = Image_data{im};
        %emptyIndex      = cellfun('isempty', X); % for replacing the empty cells by 0
        %X(emptyIndex)   = {0};
        XX              = X; %cell2table(X);
        A               = max(XX{:,4});
        fixation_dura   = [];
        
        while i<A+1
            j                       = find(XX{:,4}==i,1,'first');
            fixation_duration{i}    = XX{j,6};
            fixation_dura           = [fixation_dura;fixation_duration{i}];
            i = i+1;
        end
        
        Average_fixation_duration{im}   = mean(fixation_dura);
        Fixation_index{im}              = length(fixation_dura);
        im                              = im+1;
        clear X XX
    end
    
    Fixation_index              = cat(1,Fixation_index{:});
    Average_fixation_duration   = cat(1,Average_fixation_duration{:});
    
    %% extract the saccade index per image and the average duration
    clear Average_saccade_duration Saccade_index

    i   = 1;
    im  = 1;
    for im=1:size(Image_data,2)
        X               = Image_data{im};
        %emptyIndex      = cellfun('isempty', X); % for replacing the empty cells by 0
        %X(emptyIndex)   = {0};
        XX              = X; %cell2table(X);
        A               = max(XX{:,8});
        saccade_dura    = [];
        % saccade_direct=[];
        while i<A+1
            j                   = find(XX{:,8}==i,1,'first');
            saccade_duration{i} = XX{j,6};
            saccade_dura        = [saccade_dura;saccade_duration{i}];
            %   saccade_direction{i}=XX{j,9};
            % saccade_direct=[saccade_direct;saccade_direction{i}];
            i=i+1;
        end
        
        Average_saccade_duration{im}    = mean(saccade_dura);
        %  Average_saccade_direction{im}=mean (saccade_direct);
        Saccade_index{im}               = length(saccade_dura);
        im                              = im+1;
        clear X XX
    end
    
    Saccade_index               = cat(1,Saccade_index{:});
    Average_saccade_duration    = cat(1,Average_saccade_duration{:});
    %Average_saccade_direction=cat(1,Average_saccade_direction{:});
    
    data=[image',Presentation,Delay,dat2,Fixation_index,Average_fixation_duration,Saccade_index,Average_saccade_duration];
    
    %% Data selection
    Table{1,1}  = ['pict number'];
    Table{1,2}  = ['time presentaion'];
    Table{1,3}  = ['delay'];
    Table{1,4}  = ['vist rawA'];
    Table{1,5}  = ['A1'];
    Table{1,6}  = ['B1'];
    Table{1,7}  = ['A2'];
    Table{1,8}  = ['B2'];
    Table{1,9}  = ['fixation index'];
    Table{1,10} = ['fixation duration'];
    Table{1,11} = ['saccade index'];
    Table{1,12} = ['saccade duration'];
    %   Table{1,13}=['saccade direction'];
    
    
    %% select data in between 5 and 35s
    k       = 1;
    h       = 1;
    VPC_1   = [];
    VPC_2   = [];
    [a b]   = size (data);
    for k=1:a
        if (data(k,4)>Min) && (data(k,4)<Max)
            VPC_1(h,:)  = [data(k,:)];
            k           = k+1;
            h           = h+1;
        else
            k           = k+1;
        end
    end
    if h>1 % it means that VPC_1 is not empty, tha animal looked at one image at least
        a =size  (VPC_1);
        
        %% Create VPC_2 by selecting the data from VPC_1 where the monkey looked at the first A or B for a minimum of 0.1s
        j=1;
        
        for j=1:a(1)
            if  sum(VPC_1(j,5:6))>0
                nn      = VPC_1([j] , 1 );
                VPC_2   = [VPC_2;VPC_1([j], : )];
            end
            
            j=j+1;
        end
    end
    
    TTDuration          = cell2mat (TTDuration);
    
    N_Saccade           = sum(Saccade_index);
    N_Fixation          = sum(Fixation_index);
    Saccade_frequency   = N_Saccade/TTDuration;
    Fixation_frequency  = N_Fixation/TTDuration;
    Fixation_duration   = mean(cat (1,fixation_duration{:}));
    Saccade_duration    = mean(cat (1,saccade_duration{:}));
    
    
    
    save (fname,'data','Min','Max','VPC_1','VPC_2','Table','TTDuration','N_Saccade','N_Fixation','Saccade_frequency','Fixation_frequency','Fixation_duration','Saccade_duration')
    clear D T VPC* Time Fam20 image n dat2 dat data fixation* Fixation* N* saccade* Saccade* Average* presentation delay Presentation Delay index* image* Image* A Im TTDuration num txt raw a b
    %%
    
% end


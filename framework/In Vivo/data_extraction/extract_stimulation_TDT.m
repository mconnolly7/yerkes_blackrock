function  extract_stimulation_TDT

animal_id       = 'ARN056';
% tag             = 'bayesian_optimization';
tag             = 'grid';

experiment_date = '2017_09_20';
results_home    = ['results\' animal_id '\' tag '\']; 
% results_home    = 'results\ARN055\bayesian_optimization\';
result_dir      = 'ADMETS_I8_NOR_real_time_opt-ARN056_20170920T171304';
save_home       = 'Z:\extracted_data';

save_dir        = sprintf('%s\\%s\\%s\\%s', save_home, animal_id, experiment_date, result_dir);
tank_name       = 'CustomStimActiveX_DT1_091317';
% tank_name       = 'Z:\CustomStimActiveX_Backup\CustomStimActiveX_DT1_071717';

mkdir(save_dir);

experiment_table = readtable([results_home result_dir '\stimulation_table.csv']);
save(sprintf('%s/experiment_table_%s.mat', save_dir, tag), 'experiment_table')

% Extract data
window                  = 10;
recording_channels      = [2 4 6 8 9 11 13 15];

for c1 = 1:size(experiment_table,1)
    stim_start  = experiment_table.stimulation_time(c1);
    stim_dur    = experiment_table.stimulation_duration(c1);
    t1          = stim_start - window;
    t2          = stim_start + stim_dur + window;
    file_name   = sprintf('%s\\%s_%d.mat',save_dir,tag, c1);
   
    try
        d               = TDT2mat(tank_name, experiment_table.block_name{c1}, 'STORE', 'Wave', 'T1', t1, 'T2', t2, 'VERBOSE', 0);
        data            = d.streams.Wave.data(recording_channels,:);
        stimulation_uid = experiment_table.stimulation_uid(c1);
        
%         if ~isempty(stimulating_channels)
%             s               = TDT2mat('Data_Tank_2016_11_09', experiment_table.block_name{c1}, 'STORE', 'Sign', 'T1', t1, 'T2', t2, 'VERBOSE', 0);
%             sign            = s.streams.Sign.data(stimulating_channels,:);
%             
%         end
%         
        save_segment(file_name, data, t1, t2,stim_start, stimulation_uid)
        
    catch    
        fprintf('stimulation %d could not be extracted\n', c1);
    end
end
end

function save_segment(file_name, data, t1, t2,stim_start, stimulation_uid)

save(file_name, 'data','t1', 't2','stim_start', 'stimulation_uid');

end

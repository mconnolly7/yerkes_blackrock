function TD = connect_to_TDT()
    device_name = '';
    while strcmp(device_name,'')
        TD = actxserver('TDevAcc.X');
        TD.ConnectServer('Local');
        device_name = TD.GetDeviceName(0)
        pause(1);
    end
end
function x = aquisition_function(gp_)

f_max                       = max(gp_.y_data);
options.FunctionTolerance   = gp_.function_tolerance;

x = simulannealbnd(@(p) expected_improvement(f_max, gp_),    ...
    obj.x0, obj.lower_bound, obj.upper_bound,               ...
    options);

end


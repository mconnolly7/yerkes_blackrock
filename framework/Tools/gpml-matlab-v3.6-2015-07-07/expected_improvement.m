function ei = expected_improvement(x, f_max, gp_)

[yp, ys]    = gp_.predict(x);
z           = (yp - f_max)./ys;
ei          = -1*((yp - f_max - 0.01).*normcdf(z) + ys.*normpdf(z));

end


clear gp_model
folder = 'C:\Users\TDT\Documents\IntelligentControl\results\EPI019\bayesian_optimization\4-10Hz_fitmax_2param_OF_area_delta_trial1-EPI019_20170823T101630';
stim_table = csvread(strcat(folder,'\','parameter.csv'));
OF_1 = csvread(strcat(folder,'\','objective_function.csv'));
OF = OF_1(:,3);

%fitmax
if exist(strcat(folder,'\','second_objective_function.csv'))
OF_2 = csvread(strcat(folder,'\','second_objective_function.csv'));
OF_2 = OF_2(:,3);
OF = OF_2;
end
% lower = [3.8 2 1];
% upper = [4.4 55 10];
lower = [3.9 2];
upper = [4.2 13];

Pre_state = csvread(strcat(folder,'\','state_function.csv'));
Pre_state = Pre_state(:,:);
n_vars = size(stim_table,2);
% OF = second_metric';
N_trial = size(OF,1);
N_trial = N_trial-1;
% N_trial = 11;
gp_model = opto_gp_object();
gp_model.initialize_data(stim_table(1:N_trial,:),OF(1:N_trial),lower, upper);
gp_model.acquisition_function = 'EI';
y = gp_model.predict(gp_model.t);
% y = reshape(y,100,100);
[dum Index] = max(y);

if n_vars == 2
    figure
    gp_model.plot_mean;
    [a b] = ind2sub([100 100], Index);
    param = gp_model.t(Index,:)

elseif n_vars == 3
    [a b c] = ind2sub([100 100 100], Index);
%     param = gp_model.discrete_aquisition_function(2,0.01)
    param = gp_model.t(Index,:);
    
end


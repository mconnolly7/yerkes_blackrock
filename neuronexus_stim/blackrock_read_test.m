function blackrock_read_test 
close all
sampling_rate   = 1000;
window          = 5;
window_samples  = sampling_rate * window;
t_raw           = (1:window_samples)/sampling_rate;
params.Fs       = sampling_rate;
params.tapers   = [3 5];
params.fpass    = [1 50];

data_buffer     = circVBuf(int64(window_samples),int64(1),0);
seizure_buffer  = circVBuf(int64(200),int64(1),0);
time_buffer     = circVBuf(int64(200),int64(1),0);

cbmex('open');
cbmex('trialconfig',0)
cbmex('trialconfig',1,'absolute')
load('waldo_sz_model.mat', 'model_all_data');
load('waldo_sz_model_ensemble.mat', 'models');
t_start = [];

highpass_filt   = designfilt('highpassiir','FilterOrder',4, ...
       'HalfPowerFrequency',5, ...
       'DesignMethod','butter','SampleRate',sampling_rate);
 
notch_filt      = designfilt('bandstopiir','FilterOrder',4, ...
       'HalfPowerFrequency1',55,'HalfPowerFrequency2',65, ...
       'DesignMethod','butter','SampleRate',sampling_rate);
   
   
while 1
    
    pause(0.1);
    tic
    [a, continuousData]     = cbmex('trialdata',1);
    if isempty(t_start)
        t_start = a;
        t_current = 0;
    else
        t_current = a - t_start;
    end
    
    new_data                = continuousData{1,3};
    data_buffer.append(new_data);
    fst                     = data_buffer.fst;
    lst                     = data_buffer.lst;
    
    data = data_buffer.raw(fst:lst);
    if(size(data,1) >= window_samples)
        [S, t, f]   = mtspecgramc(data', [1 .05], params);
        
        subplot(3,1,1)
        plot_matrix(S, t, f)
        
        subplot(3,1,2)
        
        hp_data = filtfilt(highpass_filt,data);
        data    = filtfilt(notch_filt,hp_data);
        plot(t_raw, data)

        data_z      = (data - mean(data))/std(data);
        [S, f]      = mtspectrumc(data, params);
        features    = bin_data(f, S);
        for c1 = 1:20
            sz(c1)          = predict(models{c1}, features);
        end
        
        seizure_buffer.append(mean(sz));
        time_buffer.append(t_current)
        
        subplot(3,1,3)
        sz_fst  = seizure_buffer.fst;
        sz_lst  = seizure_buffer.lst;
        t_fst   = time_buffer.fst;
        t_lst   = time_buffer.lst;
        
        t_idx       = t_fst:t_lst;
        tt          = time_buffer.raw(t_fst:t_lst);
        tt_start     = t_idx(find(tt > t_current - 5, 1));
        tt          = time_buffer.raw(tt_start:t_lst);
        ss          = seizure_buffer.raw(tt_start:sz_lst);

        plot(tt,ss);

        
    end
    toc
end

end

function features =  bin_data(f, S)

for c1 = 1:49
    idx_start   = f > c1;
    idx_end     = f <= c1+1;
    
    features(1,c1) = sum(S(idx_start & idx_end));
end
end
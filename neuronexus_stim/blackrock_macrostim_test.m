function  blackrock_macrostim_test(bank_size, train_interval_on, train_interval_off, amplitude, duration, asynch_frequency, fid)


%%%%%%%%%%
% Set pulse parameters
%%%%%%%%%%
width_a     = 400;
width_b     = width_a;
amp_a       = amplitude;
amp_b       = amplitude;
interphase  = 53;
frequency   = 100; % Keep this fixed for asynchronous
n_pulses    = 1;
polarity    = 0;

%%%%%%%%%%
% Configure asynchronous pulse train
%%%%%%%%%%

% Default order of implanted electrode
[banks, bank_order]     = configure_horizontal_banks(bank_size);

n_modules               = 8; % This should be derived from the number of banks
[n_banks, n_channels]   = size(banks);
asynch_train_freq       = asynch_frequency;

% Derived values
pulse_duration          = width_a + width_b + interphase;
asynch_train_duration   = 1e6/asynch_train_freq;
inter_pulse             = asynch_train_duration/n_channels;
wait_duration           = 1000/(n_banks * asynch_train_freq) - 11; % Because it's in milliseconds...

% Seed the random number generator
% rng(0)

%%%%%%%%%%
% Configure experiment
%%%%%%%%%%
train_duration          = train_interval_on; % Duration of asynchronous pulse train (seconds)
inter_train_duration    = train_interval_off; % Duration of inter-pulse-train-interval (seconds)
n_repetitions           = floor(duration / (train_interval_on + train_interval_off)); % Times pulse train + interval are repeated
n_experiments           = 1; % Number of intermittent ADMETS + SHAM


%%% Example Experiment Sequence %%%
% train_duration          = 3; % Duration of asynchronous pulse train (seconds)
% inter_train_duration    = 3; % Duration of inter-pulse-train-interval (seconds)
% n_repetitions           = 3; % Times pulse train + interval are repeated
% n_experiments           = 2; % Number of intermittent ADMETS + SHAM
%
%             EXPERIMENT 1                           EXPERIMENT 2
% ***---***---***--- ------------------  ***---***---***--- ------------------
%

% Configure the stimulator
stimulator = configure_stimulator(n_modules);

% Setup the cleanUp function
finishup    = onCleanup(@() clean_up(stimulator));

% Program our stimulation waveform 
stimulator.setStimPattern('waveform',1,...  % We can define multiple waveforms and distinguish them by ID
    'polarity',polarity,...                 % 0=CF, 1=AF
    'pulses',n_pulses,...                   % Number of pulses in stim pattern
    'amp1',amp_a,...                        % Amplitude in uA
    'amp2',amp_b,...                        % Amplitude in uA
    'width1',width_a,...                    % Width for first phase in us
    'width2',width_b,...                    % Width for second phase in us
    'interphase',interphase,...             % Time between phases in us
    'frequency',frequency);                 % Frequency determines time between biphasic pulses

% Create a program sequence using any previously defined waveforms (we only have one)
stimulator.beginSequence;                   % Begin program definition
for c1 = 1:n_banks
    
    bank_idx = bank_order(c1);
    stimulator.beginGroup
    for c2 = 1:n_channels
        stimulator.autoStim(banks(bank_idx,c2), 1);      
    end
    stimulator.endGroup
    
    stimulator.wait(wait_duration);
end
stimulator.endSequence;         % End program definition


for c2 = 1:n_experiments
    for c1 = 1:n_repetitions
        
        % Play our program; number of repeats
        cbmex('open')
        t_start = cbmex('time');
        stimulator.play(0);
        tic
        while toc < train_duration
        
        end
        toc;
        stimulator.stop();
        
        fprintf(fid, '%f, %.2f, %.2f, %d, %.2f, %.2f, %.2f, BLOCK_%d=pattern\n', ...
             t_start, duration, amplitude, 852, asynch_frequency, train_interval_on, train_interval_off, bank_size);

        fprintf('time=%f, frequency=%d, train_interval_on=%d, train_interval_off=%d, duration=%d, pattern=BLOCK_%d\n', ...
            t_start, asynch_frequency, train_interval_on, train_interval_off, duration, bank_size)
        cbmex('close')
        
        pause(inter_train_duration);

    end
    pause((train_duration + inter_train_duration)*n_repetitions)

end

end

function stimulator = configure_stimulator(n_modules)

% Create stimulator object
stimulator = cerestim96();

% Check for stimulation
DeviceList = stimulator.scanForDevices();

% Select a stimulator
stimulator.selectDevice(DeviceList(1));

% Connect to the stimulator
stimulator.connect; 

% Activate modules
d_info = stimulator.deviceInfo();
for c1 = 1:n_modules
    if d_info.moduleStatus(c1) ~= 1
        stimulator.enableModule(c1)
    end
end

end

function clean_up(stimulator)
stimulator.stop()
stimulator.disconnect
cbmex('close')
end

function [banks, bank_order] = configure_horizontal_banks(bank_size)

channel_order = [23, 7, 10, 26, 25, 9, 8, 24, ...
    21, 5, 12, 28, 27, 11, 6, 22,...
    19, 3, 14, 30, 29, 13, 4, 20, ...
    17, 1, 16, 32, 31, 15, 2, 18];

if bank_size == 8
    banks = reshape(channel_order,8,[])';
    bank_order = [1 3 2 4];
    
elseif bank_size == 4
    banks = reshape(channel_order,4,[])';
    bank_order = [1 5 2 6 3 7 4 8];
end
  
end


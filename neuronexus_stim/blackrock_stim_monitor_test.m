function  blackrock_stim_monitor_test()
close all;
clc;

%%%%%%%%%%
% Set pulse parameters
%%%%%%%%%%
width_a     = 100;
width_b     = width_a;
amp_a       = 150;
amp_b       = amp_a;
interphase  = 53;
frequency   = 20; % Keep this fixed for asynchronous
n_pulses    = 1;
polarity    = 0;


% Configure the stimulator
% Create stimulator object
stimulator = cerestim96();

% Check for stimulation
DeviceList = stimulator.scanForDevices();

% Select a stimulator
stimulator.selectDevice(DeviceList(1));

% Connect to the stimulator
stimulator.connect; 

% Activate modules
d_info = stimulator.deviceInfo();

for c1 = 1:16
    if d_info.moduleStatus(c1) == 1
        stimulator.disableModule(c1)
    end
end

stimulator.enableModule(15)

% Program our stimulation waveform 
stimulator.setStimPattern('waveform',1,...  % We can define multiple waveforms and distinguish them by ID
    'polarity',polarity,...                 % 0=CF, 1=AF
    'pulses',n_pulses,...                   % Number of pulses in stim pattern
    'amp1',amp_a,...                        % Amplitude in uA
    'amp2',amp_b,...                        % Amplitude in uA
    'width1',width_a,...                    % Width for first phase in us
    'width2',width_b,...                    % Width for second phase in us
    'interphase',interphase,...             % Time between phases in us
    'frequency',frequency);                 % Frequency determines time between biphasic pulses

% Create a program sequence using any previously defined waveforms (we only have one)

stimulator.beginSequence;           % Begin program definition

stimulator.autoStim(1, 1);      

stimulator.endSequence;         % End program definition
stimulator.play(1000);


% Close it all
cbmex('close')
stimulator.disconnect;
clear stimulator

end


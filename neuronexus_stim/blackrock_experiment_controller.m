function blackrock_experiment_controller

subject_id          = 'casper';
experiment_date     = datestr(date, 'yyyy_mm_dd');

% experiment_type     = 'grid_search';
% experiment_type     = 'random_search';
experiment_type     = 'open_loop';
% experiment_type     = 'closed_loop';

PCN                 = 1;

%%%%
trial               = '001';
bank_size           = 4;
pattern             = 'DIST';
%%%%%

if PCN
    experiment_name = [subject_id '_PCN_' experiment_type '_' experiment_date '_' trial];
else
    experiment_name = [subject_id '_' experiment_type '_' experiment_date '_' trial];
end
fid                 = fopen([experiment_name '.csv'], 'a');

switch experiment_type
    case 'grid_search'
        train_intervals_on  = [5 10 20];
        asynch_frequencies  = [3 5 7 9 11];
        amplitudes          = 150;
        duration            = 40;
        train_interval_off  = 20; 
        params              = combvec(train_intervals_on, asynch_frequencies, amplitudes)';                    

    case 'random_search'
        train_interval_off      = 11; 
        asynch_bounds           = [3 11];
        train_interval_bounds   = [1 20];
        amplitude_bounds        = [1 150];
        n_trials                = 100;
        duration                = 30;
        
        p(1,:)                  = polyfit([0 1], train_interval_bounds,1);
        p(2,:)                  = polyfit([0 1], asynch_bounds,1);
        p(3,:)                  = polyfit([0 1], amplitude_bounds,1);
        
        for c1 = 1:size(p,1)
            params(:,c1) = polyval(p(c1,:), rand(n_trials,1));
        end
        
    case 'open_loop' 
        train_intervals_on  = 5;
        asynch_frequencies  = 7;
        amplitude           = 150;
        duration            = 10;
        train_interval_off  = 5; 
        
        params(1)           = train_intervals_on;
        params(2)           = asynch_frequencies;
        params(3)           = amplitude;
end

for c2 = 1:1
    params              = params(randperm(size(params,1)),:);
  
    for c1 = 1:size(params,1)

        train_interval_on   = params(c1,1);
        asynch_frequency    = params(c1,2);
        amplitude           = params(c1,3);
        
        switch pattern
            case 'BLOCK'
                blackrock_macrostim_test(bank_size, train_interval_on, train_interval_off, ...
                    amplitude, duration, asynch_frequency, fid)
            case 'DIST'
                blackrock_micro_stim_test(train_interval_on, train_interval_off, ...
                    amplitude, duration, asynch_frequency, fid)
        end
        
    end
end
end
function haraeus_monopolar_AD_test()
data_dir                = 'S:\ADMES\KP_data\PCN\';

date_string             = datestr(datetime,'YYYY_mm_dd');
date_time_string        = datestr(datetime,'YYYY_mm_dd-HH_MM_SS');
subject                 = 'KP';

save_dir                = [data_dir subject '_' date_string];
save_file_name          = [subject '_monopolar_ring_' date_time_string]; 
save_path               = [save_dir filesep save_file_name];

cbmex('open')
cbmex('fileconfig', save_path, '', 1)

copyfile([mfilename('fullpath') '.m'], [save_path '.m'])

stimulation_parameters  = {'stimulation_time', 'stimulation_duration', ...
    'pulse_amplitude', 'pulse_width', 'pulse_frequency', ...
    'train_duration_on', 'train_duration_off', 'configuration'};

stimulation_table       = table();

train_interval_on       = 5;
train_interval_off      = 5;
duration                = 1200;
baseline_duration       = 10;
amplitude               = 200;
configuration           = 'monopolar_ring';

%%%%%%%%%%
% Set pulse parameters for bipolar pulses
%%%%%%%%%%
width_a     = 200;
width_b     = width_a;
amp_a       = amplitude;
amp_b       = amp_a;
interphase  = 53;
frequency   = 130; % Keep this fixed for asynchronous
n_pulses    = 1;

%%%%%%%%%%
% Configure asynchronous pulse train
%%%%%%%%%%

% Default order of implanted electrode
n_modules         	= numel(8); % This should be derived from the number of banks

%%%%%%%%%%
% Configure experiment
%%%%%%%%%%
n_repetitions     	= floor(duration / (train_interval_on + train_interval_off)); % Times pulse train + interval are repeated

% Configure the stimulator
stimulator          = configure_stimulator(n_modules);

% Setup the cleanUp function
finishup            = onCleanup(@() clean_up(stimulator, save_path));

% Program our stimulation waveforms 
stimulator.setStimPattern('waveform',1,...  % We can define multiple waveforms and distinguish them by ID
    'polarity',0,...                        % 0=CF, 1=AF
    'pulses',n_pulses,...                   % Number of pulses in stim pattern
    'amp1',amp_a,...                        % Amplitude in uA
    'amp2',amp_b,...                        % Amplitude in uA
    'width1',width_a,...                    % Width for first phase in us
    'width2',width_b,...                    % Width for second phase in us
    'interphase',interphase,...             % Time between phases in us
    'frequency',frequency);                 % Frequency determines time between biphasic pulses

% Create a program sequence using any previously defined waveforms
stimulator.beginSequence;       % Begin program definition
stimulator.beginGroup()

% Anterior electrode
stimulator.autoStim(2,1);       
stimulator.autoStim(8,1); 
stimulator.autoStim(14,1);      
stimulator.autoStim(20,1);      

% Posterior electrode
stimulator.autoStim(1,1);      
stimulator.autoStim(7,1);
stimulator.autoStim(13,1);      
stimulator.autoStim(19,1);

stimulator.endGroup() 

stimulator.endSequence;         % End program definition

%%%%%%%
% Run the experiment
%%%%%%%
pause(baseline_duration)
for c1 = 1:n_repetitions
        
    t_start = cbmex('time');
    
    % Play our program; number of repeats
    stimulator.play(0);
    pause(train_interval_on);
    stimulator.stop();

    stimulation_row = table(t_start, duration, amplitude, width_a, frequency, ...
        train_interval_on, train_interval_off, {configuration}, 'VariableNames', stimulation_parameters);
    
    stimulation_table = [stimulation_table; stimulation_row];
    writetable(stimulation_table, [save_path '.csv'])
    
    pause(train_interval_off);

end
pause(duration)
end

function stimulator = configure_stimulator(n_modules)

% Create stimulator object
stimulator = cerestim96();

% Check for stimulation
DeviceList = stimulator.scanForDevices();

% Select a stimulator
stimulator.selectDevice(DeviceList(1));

% Connect to the stimulator
stimulator.connect; 

% Activate modules
d_info = stimulator.deviceInfo();
for c1 = 1:n_modules
    if d_info.moduleStatus(c1) ~= 1
        stimulator.enableModule(c1)
    end
end

end

function clean_up(stimulator, save_path)
stimulator.stop()
stimulator.disconnect()

cbmex('fileconfig', save_path, '', 0)
cbmex('close')
end



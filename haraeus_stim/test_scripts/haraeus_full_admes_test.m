function  haraeus_full_admes(train_interval_on, train_interval_off, amplitude, duration, asynch_frequency, fid)

%%%%%%%%%%
% Set experiment parameters
%%%%%%%%%%
train_interval_on   = 5;
train_interval_off  = 5;
duration            = 12005;
amplitude           = 200;
asynch_frequency    = 7;

%%%%%%%%%%
% Set pulse parameters
%%%%%%%%%%
width_a     = 200;
width_b     = width_a;
amp_a       = amplitude;
amp_b       = amp_a;
interphase  = 53;
frequency   = 1500; % Keep this fixed for asynchronous
n_pulses    = 1;
polarity    = 0;

%%%%%%%%%%
% Configure asynchronous pulse train
%%%%%%%%%%

% Default order of implanted electrode
electrode_ant           = 1:2:24;
electrode_pst           = 2:2:24;

n_modules               = 8; % This should be derived from the number of banks
n_channels              = size(electrode_ant,2);
asynch_train_freq       = asynch_frequency;

% Derived values
pulse_duration          = width_a + width_b + interphase;
asynch_train_duration   = 1e6/asynch_train_freq;
inter_pulse             = asynch_train_duration/n_channels;
wait_duration           = 1000/(n_channels * asynch_train_freq) - 11; % Because it's in milliseconds...who knows why

% Seed the random number generator
rng(0)
stim_order              = randperm(24);
% stim_order_ant              = randperm(n_channels);
% stim_order_pst              = randperm(n_channels);

%%%%%%%%%%
% Configure experiment
%%%%%%%%%%
train_duration          = train_interval_on; % Duration of asynchronous pulse train (seconds)
inter_train_duration    = train_interval_off; % Duration of inter-pulse-train-interval (seconds)
n_repetitions           = floor(duration / (train_interval_on + train_interval_off)); % Times pulse train + interval are repeated
n_experiments           = 1 ; % Number of intermittent ADMETS + SHAM

 
%%% Example Experiment Sequence %%%
% train_duration          = 3; % Duration of asynchronous pulse train (seconds)
% inter_train_duration    = 3; % Duration of inter-pulse-train-interval (seconds)
% n_repetitions           = 3; % Times pulse train + interval are repeated
% n_experiments           = 2; % Number of intermittent ADMETS + SHAM
%
%             EXPERIMENT 1                           EXPERIMENT 2
% ***---***---***--- ------------------  ***---***---***--- ------------------
%

% Configure the stimulator
stimulator = configure_stimulator(n_modules);

% Setup the cleanUp function
finishup    = onCleanup(@() clean_up(stimulator));

% Program our stimulation waveform 
stimulator.setStimPattern('waveform',1,...  % We can define multiple waveforms and distinguish them by ID
    'polarity',polarity,...                 % 0=CF, 1=AF
    'pulses',n_pulses,...                   % Number of pulses in stim pattern
    'amp1',amp_a,...                        % Amplitude in uA
    'amp2',amp_b,...                        % Amplitude in uA
    'width1',width_a,...                    % Width for first phase in us
    'width2',width_b,...                    % Width for second phase in us
    'interphase',interphase,...             % Time between phases in us
    'frequency',frequency);                 % Frequency determines time between biphasic pulses

% Create a program sequence using any previously defined waveforms (we only have one)

stimulator.beginSequence;                   % Begin program definition

for c1 = 1:n_channels
    stimulator.beginGroup
    
%     channel_1 = electrode_ant(stim_order_ant(c1));
%     channel_2 = electrode_pst(stim_order_pst(c1));
    
    % Headstage  1
    stimulator.autoStim(stim_order, 1);      
%     stimulator.autoStim(channel_2, 1);

    stimulator.endGroup
%     stimulator.wait(1);
end

stimulator.endSequence;         % End program definition

% Log stimulation start
for c2 = 1:n_experiments
    for c1 = 1:n_repetitions
        
        % Play our program; number of repeats
        cbmex('open')
        t_start = cbmex('time');
        
        stimulator.play(0);
        tic
        while toc < train_duration
        
        end
        stimulator.stop()
        toc
        
        fprintf('time=%f, frequency=%d, train_interval_on=%d, train_interval_off=%d, duration=%d, pattern=DIST_%d\n', ...
            t_start, asynch_frequency, train_interval_on, train_interval_off, duration, 4)
        cbmex('close')
        
        pause(inter_train_duration);

    end
    pause((train_duration + inter_train_duration)*n_repetitions)

end

% Close it all
cbmex('close')
stimulator.disconnect;
clear stimulator

end

function stimulator = configure_stimulator(n_modules)

% Create stimulator object
stimulator = cerestim96();

% Check for stimulation
DeviceList = stimulator.scanForDevices();

% Select a stimulator
stimulator.selectDevice(DeviceList(1));

% Connect to the stimulator
stimulator.connect; 

% Activate modules
d_info = stimulator.deviceInfo();
for c1 = 1:n_modules
    if d_info.moduleStatus(c1) ~= 1
        stimulator.enableModule(c1)
    end
end

end

function clean_up(stimulator)
stimulator.stop()
stimulator.disconnect
cbmex('close')
fclose('all')
end
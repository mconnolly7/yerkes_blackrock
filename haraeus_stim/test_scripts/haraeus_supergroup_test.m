function  stimulation_table = haraeus_supergroup(train_interval_on, train_interval_off, amplitude, train_duration, program_wait)

train_interval_on   = 5;
train_interval_off  = 5;
train_duration      = 10;
amplitude           = 1000;
program_wait        = 6;

%%%%%%%%%%
% Set up stimulation table
%%%%%%%%%%
% var_names           = {'stimulation_start_time', 'train_duration', 'amplitude_a', 'amplitude_b', 'program_wait', 'interphase_interval', ...
%         'pulse_width_a', 'pulse_width_b', 'train_interval_on', 'train_interval_off', 'electrode_configuration'};
% stimulation_table   = table([],'VariableNames', var_names);
stimulation_table   = [];
%%%%%%%%%%
% Set parameters for bipolar pulses
%%%%%%%%%%
pulse_width_a                 = 200;
pulse_width_b                 = pulse_width_a;
amplitude_a             = amplitude;
amplitude_b             = amplitude_a;
interphase_interval     = 53;
frequency               = 1000; % This DOES control the overall stimulation frequency
n_pulses                = 1;
electrode_configuration = {'wide_bipolar'}; % Needs to be a cell for the table entry below

%%%%%%%%%%
% Configure asynchronous pulse train
%%%%%%%%%%

% Default order of implanted electrode
anterior_ventral        = 13;
anterior_dorsal         = 1;
posterior_ventral       = 2;
posterior_dorsal        = 22;

n_modules               = 4; 

%%%%%%%%%%
% Configure experiment
%%%%%%%%%%
n_repetitions           = floor(train_duration / (train_interval_on + train_interval_off)); % Times pulse train + interval are repeated

% Configure the stimulator
stimulator = configure_stimulator(n_modules);

% Setup the cleanUp function
finishup    = onCleanup(@() clean_up(stimulator));

% Program our stimulation waveforms 
stimulator.setStimPattern('waveform',1,...  % We can define multiple waveforms and distinguish them by ID
    'polarity',0,...                        % 0=CF, 1=AF
    'pulses',n_pulses,...                   % Number of pulses in stim pattern
    'amp1',amplitude_a,...                        % Amplitude in uA
    'amp2',amplitude_b,...                        % Amplitude in uA
    'width1',pulse_width_a,...                    % Width for first phase in us
    'width2',pulse_width_b,...                    % Width for second phase in us
    'interphase',interphase_interval,...             % Time between phases in us
    'frequency',frequency);                 % Frequency determines time between biphasic pulses

stimulator.setStimPattern('waveform',2,...  % We can define multiple waveforms and distinguish them by ID
    'polarity',1,...                        % 0=CF, 1=AF
    'pulses',n_pulses,...                   % Number of pulses in stim pattern
    'amp1',amplitude_a,...                        % Amplitude in uA
    'amp2',amplitude_b,...                        % Amplitude in uA
    'width1',pulse_width_a,...                    % Width for first phase in us
    'width2',pulse_width_b,...                    % Width for second phase in us
    'interphase',interphase_interval,...             % Time between phases in us
    'frequency',frequency);                 % Frequency determines time between biphasic pulses


% Create a program sequence using any previously defined waveforms
stimulator.beginSequence;       % Begin program definition

stimulator.beginGroup()
stimulator.autoStim(posterior_dorsal,1);      
stimulator.autoStim(posterior_ventral,2);       
stimulator.endGroup() 

% stimulator.beginGroup()
% stimulator.autoStim(anterior_ventral,1);      
% stimulator.autoStim(anterior_dorsal,2);   %%%% This is normally channel 1, but today (2019/09/03) is 13   
% stimulator.endGroup()
stimulator.wait(0);

stimulator.endSequence;         % End program definition

%%%%%%% 
% 
%%%%%%%
for c1 = 1:n_repetitions
        
    cbmex('open')
    t_start = cbmex('time');
    
    % Play our program; number of repeats
    stimulator.play(0);
    pause(train_interval_on);
    stimulator.stop();

    stimulation_row = table(t_start, train_duration, amplitude_a, amplitude_b, program_wait, interphase_interval, ...
        pulse_width_a, pulse_width_b, train_interval_on, train_interval_off, electrode_configuration);
    
    stimulation_table = [stimulation_table; stimulation_row];
    stimulation_row
    
    cbmex('close')

    pause(train_interval_off);

end

% pause(train_duration)
end

function stimulator = configure_stimulator(n_modules)

% Create stimulator object
stimulator = cerestim96();

% Check for stimulation
DeviceList = stimulator.scanForDevices();

% Select a stimulator
stimulator.selectDevice(DeviceList(1));

% Connect to the stimulator
stimulator.connect; 

% Activate modules
d_info = stimulator.deviceInfo();
for c1 = 1:n_modules
    if d_info.moduleStatus(c1) ~= 1
        stimulator.enableModule(c1)
    end
end

end

function clean_up(stimulator)
stimulator.stop()
stimulator.disconnect
cbmex('close')
end




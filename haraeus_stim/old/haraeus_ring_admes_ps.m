function  haraeus_ring_admes_ps(train_interval_on, train_interval_off, amplitude, duration, asynch_frequency, fid)
% 
% train_interval_on   = 5;
% train_interval_off  = 5;
% duration            = 1200;
% amplitude           = 500; % ONLY CHANGE THIS!!!!
% asynch_frequency    = 7;

%%%%%%%%%%
% Set pulse parameters for bipolar  pulses
%%%%%%%%%%
width_a     = 200;
width_b     = width_a;
amp_a       = amplitude;
amp_b       = amp_a;
interphase  = 53;
frequency   = 100; % Keep this fixed for asynchronous
n_pulses    = 1;

%%%%%%%%%%
% Configure asynchronous pulse train
%%%%%%%%%%

% Default order of implanted electrode
channel_order           = [20, 14; 7, 1; 8, 2; 19, 13];
n_modules               = numel(channel_order); % This should be derived from the number of banks
n_groups                = size(channel_order,1);
wait_duration           = 1000 / n_groups / asynch_frequency - 11;

%%%%%%%%%%
% Configure experiment
%%%%%%%%%%
n_repetitions           = floor(duration / (train_interval_on + train_interval_off)); % Times pulse train + interval are repeated

% Configure the stimulator
stimulator = configure_stimulator(n_modules);

% Setup the cleanUp function
finishup    = onCleanup(@() clean_up(stimulator));

% Program our stimulation waveforms 
stimulator.setStimPattern('waveform',1,...  % We can define multiple waveforms and distinguish them by ID
    'polarity',0,...                        % 0=CF, 1=AF
    'pulses',n_pulses,...                   % Number of pulses in stim pattern
    'amp1',amp_a,...                        % Amplitude in uA
    'amp2',amp_b,...                        % Amplitude in uA
    'width1',width_a,...                    % Width for first phase in us
    'width2',width_b,...                    % Width for second phase in us
    'interphase',interphase,...             % Time between phases in us
    'frequency',frequency);                 % Frequency determines time between biphasic pulses

stimulator.setStimPattern('waveform',2,...  % We can define multiple waveforms and distinguish them by ID
    'polarity',1,...                        % 0=CF, 1=AF
    'pulses',n_pulses,...                   % Number of pulses in stim pattern
    'amp1',amp_a,...                        % Amplitude in uA
    'amp2',amp_b,...                        % Amplitude in uA
    'width1',width_a,...                    % Width for first phase in us
    'width2',width_b,...                    % Width for second phase in us
    'interphase',interphase,...             % Time between phases in us
    'frequency',frequency);                 % Frequency determines time between biphasic pulses


% Create a program sequence using any previously defined waveforms
stimulator.beginSequence;                   % Begin program definition
for c1 = 1:size(channel_order,n_groups)
    
    stimulator.beginGroup()
    stimulator.autoStim(channel_order(c1, 1),1);      
    stimulator.autoStim(channel_order(c1, 2),2);      
    stimulator.endGroup()

    stimulator.wait(wait_duration);
end
stimulator.endSequence;         % End program definition

%%%%%%%
%
%%%%%%%
for c1 = 1:n_repetitions
        
    cbmex('open')
    t_start = cbmex('time');
    
    % Play our program; number of repeats
    stimulator.play(0);
    pause(train_interval_on);
    stimulator.stop();

   fprintf(fid, '%f,     %.2f,      %.2f,      %f,              %.2f,             %.2f,              %.2f,               %s\n', ...
              t_start, duration, amplitude, width_a+width_b, asynch_frequency, train_interval_on, train_interval_off, 'RING_ADMES');

    fprintf('time=%f, frequency=%d, train_interval_on=%d, train_interval_off=%d, duration=%d, pattern=RING_ADMES\n', ...
        t_start, asynch_frequency, train_interval_on, train_interval_off, duration)
    cbmex('close')

    pause(train_interval_off);

end
end

function stimulator = configure_stimulator(n_modules)

% Create stimulator object
stimulator = cerestim96();

% Check for stimulation
DeviceList = stimulator.scanForDevices();

% Select a stimulator
stimulator.selectDevice(DeviceList(1));

% Connect to the stimulator
stimulator.connect; 

% Activate modules
d_info = stimulator.deviceInfo();
for c1 = 1:n_modules
    if d_info.moduleStatus(c1) ~= 1
        stimulator.enableModule(c1)
    end
end

end

function clean_up(stimulator)
stimulator.stop()
stimulator.disconnect
cbmex('close')
end


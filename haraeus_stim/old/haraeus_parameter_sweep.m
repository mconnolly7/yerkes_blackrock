clear
subject_id          = 'KP';
experiment_date     = datestr(datetime, 'yyyy_mm_dd-HH_MM_SS');
stimulation_table   = [];
log_path            = ['S:\ADMES\KP_data\KP_2020_03_04' filesep subject_id '_' experiment_date filesep];
param_f             = [4 6 15 70];

n_trials            = 40;
n_params            = size(param_f,2);
n_reps              = n_trials / n_params;
param_f_ordered     = repmat(param_f, 1, n_reps);

rand_idx            = randperm(size(param_f_ordered,2));
param_f_rand        = param_f_ordered(rand_idx);

train_interval_on   = 5;
train_interval_off  = 15;
amplitude           = 1000;
duration            = 20;

for c1 = 1:size(param_f_rand,2)
    fprintf('Sample: %d\n', c1)
    
    program_wait = param_f_rand(c1);
    stimulation_row = haraeus_supergroup(train_interval_on, train_interval_off, amplitude, duration, program_wait);
    
    stimulation_table = [stimulation_table; stimulation_row];
end

save(log_path,'stimulation_table')
 


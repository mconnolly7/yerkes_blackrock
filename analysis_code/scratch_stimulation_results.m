close all

d2 = [27,158,119
217,95,2]/255;

f = figure( 'Position',  [1900, 100, 1000, 800]);

x_grid          = [.07 .56];
y_grid          = [.05  .35 .65];

width           = .4;
height          = [.28 .88] ;

ax(1)        	= axes('Position', [x_grid(1)  y_grid(1) width(1) height(1)]);
ax(2)        	= axes('Position', [x_grid(1)  y_grid(2) width(1) height(1)]);
ax(3)        	= axes('Position', [x_grid(1)  y_grid(3) width(1) height(1)]);
ax(4)        	= axes('Position', [x_grid(2)  y_grid(1) width(1) height(2)]);

hold(ax(1),'on');hold(ax(2),'on');hold(ax(3),'on');hold(ax(4),'on');

data_table  = readtable('/Users/mconn24/Box Sync/UG3 Temp/NHP_update_medtronic_CRP/stimulation_results.xlsx');

group_stats = grpstats(data_table(:,2:end), 'group', {'mean','sem'});

%%%%%%%
set(f, 'currentaxes', ax(3));
y_data = [group_stats.mean_n_seizure_stim group_stats.mean_n_seizure_post];
e_data = [group_stats.sem_n_seizure_stim group_stats.sem_n_seizure_post];

b = barwitherr(e_data,y_data);
b(1).FaceColor = d2(1,:);
b(2).FaceColor = d2(2,:);

box off
xticklabels({})
yl(3) = ylabel('Number of Seizures');
yl(3).Position(1)   = .17;

set(gca,'FontSize', 16)
legend({'During Stimulation', 'Post Stimulation'}, 'Box', 'off', 'Location','northwest')
xticks([1 2 3 4])
title('Summary Data')
%%%%%%%
set(f, 'currentaxes', ax(2));
y_data = [group_stats.mean_duration_stim group_stats.mean_duration_post];
e_data = [group_stats.sem_duration_stim group_stats.sem_duration_post];

b = barwitherr(e_data,y_data);
b(1).FaceColor = d2(1,:);
b(2).FaceColor = d2(2,:);
hold on

plot([2.75 3.25], [125 125], 'k', 'LineWidth',2)
plot(3, 135, 'k*')
box off
xticklabels({})
yl(2)               = ylabel('Seizure Duration (seconds)');
yl(2).Position(1)   = .17;

set(gca,'FontSize', 16)
xticks([1 2 3 4])

%%%%%%%
set(f, 'currentaxes', ax(1));
y_data = [group_stats.mean_n_seizure_stim group_stats.mean_n_seizure_post];
e_data = [group_stats.sem_n_seizure_stim group_stats.sem_n_seizure_post];

b = barwitherr(e_data,y_data);
b(1).FaceColor = d2(1,:);
b(2).FaceColor = d2(2,:);
box off
xticklabels({})
yl(1) = ylabel('% Time in Seizure');
yl(1).Position(1)   = .17;

set(gca,'FontSize', 16)
xticks([1 2 3 4])
xticklabels({'Ring', 'Wide Ring', 'Dual Ring' 'Full ADMES'})
% xtickangle(25)

%%%%%%%
%
set(f, 'currentaxes', ax(4));

group_idx           = data_table.group == 2;
stim_duration       = data_table.duration_stim(group_idx);
post_duration       = data_table.duration_post(group_idx);

X = ones(size(stim_duration));
rng(2)
jitter_1 = (rand(size(X,1),1)-.5)/4;
jitter_2 = (rand(size(X,1),1)-.5)/4;
plot([X+jitter_1 X*2+jitter_2]', [stim_duration post_duration]','k', 'LineWidth',2)

scatter(X+jitter_1,stim_duration, 300,  .5*[1 1 1],'filled', 'MarkerFaceAlpha', 1, 'MarkerEdgeColor', 'k', 'MarkerFaceColor', d2(1,:))
scatter(X*2+jitter_2,post_duration, 300,  .5*[1 1 1],'filled', 'MarkerFaceAlpha', 1,'MarkerEdgeColor', 'k', 'MarkerFaceColor', d2(2,:))

box off
xlim([.75 2.25])
ylim([-5 220])
set(gca,'FontSize', 16)
xticks([1 2])
xticklabels({'During Stimulation', 'Post Stimulation'})
ylabel('Seizure Duration (seconds)');
title('Dual Ring Bipolar')

y_coords        = [.89  ];
x_coords        = [.02 .5];
sublabel_size   = 24;

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','A','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','B','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
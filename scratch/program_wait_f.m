plot((1:length(NS6.Data(29,:)))/30000,NS6.Data(29,:))

%%
fs      = 30000;
t_start = 146;
t_end   = t_start+1;
d       = NS6.Data(29,t_start*fs:t_end*fs);
plot((1:length(d))/30,d)



%%
1000/(109.2-28.87) % 70ms
1000/(334.8-320.7) % 4ms
1000/(521.9-496.8) % 15ms
1000/(376.6-360.6) % 6ms
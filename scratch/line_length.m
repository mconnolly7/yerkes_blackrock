
data    = KP_2019_10_07_002_ns3_Ch7.values;
t       = (1:length(data))/2000;

%%
for c1 = 2001:size(data,1)
    ll(c1) = mean(abs(data(c1-2000:c1)));
end

%%
yyaxis left
plot(t,ll)
yyaxis right
plot(t,data)
%% Load the data (this can take about a minute)
[file_name, path_name] = uigetfile('*.mkv');
file_root = [path_name file_name(1:end-4)];

v = VideoReader([file_root '.mkv']);
openNSx([file_root '.ns2'])
%%
fs              = NS2.MetaTags.SamplingFreq;
if iscell(NS2.Data)
    data = []; 
    for c1 = 1:size(NS2.Data,2)
        data = [data NS2.Data{c1}(1,:)];
    end
else
    data = NS2.Data;
end
data            = double(data);
data            = data(1,:);

%% Filter the data (get rid of post-stimulation artifacts)
highpass_filt   = designfilt('highpassiir','FilterOrder',4, ...
       'HalfPowerFrequency',5, ...
       'DesignMethod','butter','SampleRate',fs);
 
notch_filt      = designfilt('bandstopiir','FilterOrder',4, ...
       'HalfPowerFrequency1',55,'HalfPowerFrequency2',65, ...
       'DesignMethod','butter','SampleRate',fs);

hp_data = filtfilt(highpass_filt,data);
data    = filtfilt(notch_filt,hp_data);


%% Plot the data
tt = (1:length(data))/fs;
plot(tt,data)
xlabel('Seconds')
ylabel('Amplitude (uV)')
set(gca, 'FontSize', 16);

%% Play the video
% Video will not show until t > 5
START_TIME = 50;

if START_TIME > v.Duration
    msgbox(sprintf('START_TIME must be less than video duration (%.0f)', v.Duration))
    return;
end

v.CurrentTime   = START_TIME;

figure('pos', [10 90 600 900])
while hasFrame(v)
    t = v.CurrentTime;
    if t > 5
        start_idx   = (t - 5)*fs;
        end_idx     = t*fs;
        tt          = (start_idx:end_idx)/fs;
        
        subplot(2,1,1)
        plot(tt, data(int32(start_idx):int32(end_idx)));
        xlim([tt(1) tt(end)]);
        ylim([-800 800]);
    end
    
    subplot(2,1,2)
s
    vidFrame = readFrame(v);
    image(vidFrame);
%     pause(1/v.FrameRate); 
end